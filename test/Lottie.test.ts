import chai, { expect } from "chai";
import { solidity } from "ethereum-waffle";
import chaiAsPromised from "chai-as-promised";
import { GetRandom, Lottie } from "../types";
import getVmError from "./getVmError";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ethers, network } from "hardhat";
import { parseEther } from "ethers/lib/utils";

ethers.utils.Logger.setLogLevel(ethers.utils.Logger.levels.ERROR);

chai.use(chaiAsPromised);
chai.use(solidity);

const TICKET_PRICE = parseEther("0.001");
const STAKE = parseEther("0.1");
const ENDS_AT = Date.now() + 60 * 60;

describe("Lottie", () => {
  let getRandomMock: GetRandom;

  beforeEach(async () => {
    const GetRandomMock = await ethers.getContractFactory("GetRandomMock");
    getRandomMock = await GetRandomMock.deploy();
  });

  afterEach(async () => {
    // reset the network
    await network.provider.request({
      method: "hardhat_reset",
      params: [],
    });
  });

  describe("Wrong initialization", () => {
    it("should reject if no stake is provided", async () => {
      const Lottie = await ethers.getContractFactory("Lottie");

      await expect(
        Lottie.deploy(TICKET_PRICE, ENDS_AT, getRandomMock.address)
      ).to.be.rejectedWith(getVmError("Incorrect stake amount."));
    });

    it("should reject if stake amount is incorrect", async () => {
      const Lottie = await ethers.getContractFactory("Lottie");

      await expect(
        Lottie.deploy(TICKET_PRICE, ENDS_AT, getRandomMock.address, {
          value: STAKE.sub(1),
        })
      ).to.be.rejectedWith(getVmError("Incorrect stake amount."));
    });
  });

  describe("Successful initialization", () => {
    let lottie: Lottie;
    let owner: SignerWithAddress;
    let bob: SignerWithAddress;
    let alice: SignerWithAddress;

    before(async () => {
      [owner, bob, alice] = await ethers.getSigners();
    });

    beforeEach(async () => {
      const Lottie = await ethers.getContractFactory("Lottie");
      lottie = await Lottie.deploy(
        TICKET_PRICE,
        ENDS_AT,
        getRandomMock.address,
        {
          value: STAKE,
        }
      );

      await lottie.deployed();
    });

    describe("Ticket selling", () => {
      it("should be open, with proper initial states", async () => {
        expect(await lottie.isClosed()).to.be.false;
        expect(await lottie.endsAt()).to.eq(ENDS_AT);
        expect(await lottie.ticketPrice()).to.eq(TICKET_PRICE);
        expect(await lottie.soldTicketCount()).to.eq(0);
        expect(await lottie.distinctUserCount()).to.eq(0);
      });

      it("should allow users to buy tickets", async () => {
        await lottie.connect(bob).buyTickets(2, {
          value: TICKET_PRICE.mul(2),
        });

        expect(await lottie.soldTicketCount()).to.eq(2);
        expect(await lottie.distinctUserCount()).to.eq(1);
        expect(await lottie.userTickets(bob.address)).to.eq(2);
        expect(await lottie.distinctUsers(0)).to.eq(bob.address);
        expect(await lottie.tickets(0)).to.eq(bob.address);
        expect(await lottie.tickets(1)).to.eq(bob.address);
      });

      it("should reject on ticket count and price missmatch", async () => {
        await expect(lottie.buyTickets(2)).to.be.rejectedWith(
          getVmError("Ticket price error.")
        );

        await expect(
          lottie.buyTickets(2, {
            value: TICKET_PRICE.mul(3),
          })
        ).to.be.rejectedWith(getVmError("Ticket price error."));
      });
    });

    describe("Owner closes the game normally", () => {
      it("should reject close attempts before expiration", async () => {
        await expect(lottie.closeGame()).to.be.rejectedWith(
          getVmError("Game can not be closed yet.")
        );
      });

      it("should close the game propery with zero players", async () => {
        await network.provider.send("evm_setNextBlockTimestamp", [
          9999999999999,
        ]);

        const ownerBalanceBeforeRefund = await ethers.provider.getBalance(
          owner.address
        );

        await lottie.closeGame();
        expect(await lottie.isClosed()).to.be.true;

        const ownerBalanceAfterRefund = await ethers.provider.getBalance(
          owner.address
        );

        // some money lost due to the gas cost
        expect(ownerBalanceAfterRefund)
          .to.be.gt(ownerBalanceBeforeRefund)
          .and.lt(ownerBalanceBeforeRefund.add(STAKE));

        await expect(lottie.withdrawStake()).to.be.rejectedWith(
          getVmError("Nothing to withdraw.")
        );
      });

      it("should close the game properly", async () => {
        const prize = TICKET_PRICE.mul(6).div(100).mul(99);

        // a new, long running contract
        const Lottie = await ethers.getContractFactory("Lottie");
        const longRunningLottie = await Lottie.deploy(
          TICKET_PRICE,
          99999999999991,
          getRandomMock.address,
          {
            value: STAKE,
          }
        );

        await longRunningLottie.deployed();
        await network.provider.send("evm_setNextBlockTimestamp", [
          9999999999999,
        ]);

        await longRunningLottie.connect(bob).buyTickets(3, {
          value: TICKET_PRICE.mul(3),
        });

        await longRunningLottie.connect(alice).buyTickets(3, {
          value: TICKET_PRICE.mul(3),
        });

        const aliceBalanceBeforeWinning = await ethers.provider.getBalance(
          alice.address
        );

        // after expiry
        await network.provider.send("evm_setNextBlockTimestamp", [
          99999999999992,
        ]);

        await longRunningLottie.closeGame();
        expect(await longRunningLottie.isClosed()).to.be.true;

        const terminator = await longRunningLottie.terminator();
        expect(terminator[0]).to.eq(owner.address);

        const winner = await longRunningLottie.winner();
        expect(winner[0]).to.eq(alice.address);
        expect(winner[1]).to.eq(3);
        // 99% of the 6 tickets' price
        expect(winner[2]).to.eq(prize);
        expect(winner[3]).to.be.gt(9999999999999);

        const aliceBalanceAfterWinning = await ethers.provider.getBalance(
          alice.address
        );

        expect(aliceBalanceBeforeWinning).to.eq(
          aliceBalanceAfterWinning.sub(prize)
        );
      });

      it("should close the game properly with only 1 user", async () => {
        const prize = TICKET_PRICE.mul(3);

        await lottie.connect(alice).buyTickets(3, {
          value: TICKET_PRICE.mul(3),
        });

        await network.provider.send("evm_setNextBlockTimestamp", [
          9999999999999,
        ]);

        const aliceBalanceBeforeWinning = await ethers.provider.getBalance(
          alice.address
        );

        await lottie.closeGame();
        expect(await lottie.isClosed()).to.be.true;

        const terminator = await lottie.terminator();
        expect(terminator[0]).to.eq(owner.address);

        const winner = await lottie.winner();
        expect(winner[0]).to.eq(alice.address);
        expect(winner[1]).to.eq(0);
        expect(winner[2]).to.eq(prize);
        expect(winner[3]).to.be.gte(9999999999999);

        const aliceBalanceAfterWinning = await ethers.provider.getBalance(
          alice.address
        );

        expect(aliceBalanceBeforeWinning).to.eq(
          aliceBalanceAfterWinning.sub(prize)
        );
      });

      it("should not be closed twice", async () => {
        await network.provider.send("evm_setNextBlockTimestamp", [ENDS_AT + 1]);
        await lottie.closeGame();

        expect(await lottie.isClosed()).to.be.true;

        await expect(lottie.closeGame()).to.be.rejectedWith(
          getVmError("Game is closed.")
        );
      });
    });

    describe("User closes an expired game", () => {
      it("should reject close attempts before expiration + 1 day", async () => {
        await lottie.connect(bob).buyTickets(3, {
          value: TICKET_PRICE.mul(3),
        });

        await expect(lottie.connect(bob).closeGame()).to.be.rejectedWith(
          getVmError("Game can not be closed yet.")
        );
      });

      it("should reject close attempts by unknown players", async () => {
        await expect(lottie.connect(bob).closeGame()).to.be.rejectedWith(
          getVmError("Permission denied.")
        );
      });

      it("should close the game properly", async () => {
        const prize = TICKET_PRICE.mul(6);

        // a new, long running contract
        const Lottie = await ethers.getContractFactory("Lottie");
        const longRunningLottie = await Lottie.deploy(
          TICKET_PRICE,
          99999999999990,
          getRandomMock.address,
          {
            value: STAKE,
          }
        );

        await longRunningLottie.deployed();

        // after 1 day of expiry
        await network.provider.send("evm_setNextBlockTimestamp", [
          9999999999999,
        ]);

        await longRunningLottie.connect(bob).buyTickets(3, {
          value: TICKET_PRICE.mul(3),
        });

        await longRunningLottie.connect(alice).buyTickets(3, {
          value: TICKET_PRICE.mul(3),
        });

        const aliceBalanceBeforeWinning = await ethers.provider.getBalance(
          alice.address
        );

        // after 1 day of expiry
        await network.provider.send("evm_setNextBlockTimestamp", [
          99999999999990 + 24 * 60 * 60 + 1,
        ]);

        await longRunningLottie.connect(bob).closeGame();
        expect(await longRunningLottie.isClosed()).to.be.true;

        const terminator = await longRunningLottie.terminator();
        expect(terminator[0]).to.eq(bob.address);

        const winner = await longRunningLottie.winner();
        expect(winner[0]).to.eq(alice.address);
        expect(winner[1]).to.eq(3);
        expect(winner[2]).to.eq(prize);
        expect(winner[3]).to.be.gt(9999999999999);

        const aliceBalanceAfterWinning = await ethers.provider.getBalance(
          alice.address
        );

        expect(aliceBalanceBeforeWinning).to.eq(
          aliceBalanceAfterWinning.sub(prize)
        );
      });

      it("should close the game properly with only 1 player", async () => {
        const prize = TICKET_PRICE.mul(3);
        const blockTimestamp = ENDS_AT + 24 * 60 * 60 + 1;

        await lottie.connect(alice).buyTickets(3, {
          value: TICKET_PRICE.mul(3),
        });

        const aliceBalanceBeforeWinning = await ethers.provider.getBalance(
          alice.address
        );

        await network.provider.send("evm_setNextBlockTimestamp", [
          blockTimestamp,
        ]);

        await lottie.connect(alice).closeGame();
        expect(await lottie.isClosed()).to.be.true;

        const terminator = await lottie.terminator();
        expect(terminator[0]).to.eq(alice.address);

        const winner = await lottie.winner();
        expect(winner[0]).to.eq(alice.address);
        expect(winner[1]).to.eq(0);
        expect(winner[2]).to.eq(prize);
        expect(winner[3]).to.be.gte(blockTimestamp);

        const aliceBalanceAfterWinning = await ethers.provider.getBalance(
          alice.address
        );

        // some money lost due to the gas cost
        expect(aliceBalanceAfterWinning)
          .to.be.gte(aliceBalanceBeforeWinning)
          .and.lte(aliceBalanceBeforeWinning.add(prize));
      });

      it("should not be closed twice", async () => {
        await lottie.connect(bob).buyTickets(3, {
          value: TICKET_PRICE.mul(3),
        });

        await network.provider.send("evm_setNextBlockTimestamp", [
          ENDS_AT + 24 * 60 * 60 + 1,
        ]);

        await lottie.connect(bob).closeGame();

        expect(await lottie.isClosed()).to.be.true;

        await expect(lottie.connect(bob).closeGame()).to.be.rejectedWith(
          getVmError("Game is closed.")
        );
      });
    });

    describe("Closed game", () => {
      it("should reject ticket buy attempts once game is closed", async () => {
        await network.provider.send("evm_setNextBlockTimestamp", [ENDS_AT + 1]);
        await lottie.closeGame();

        await expect(
          lottie.buyTickets(2, {
            value: TICKET_PRICE.mul(2),
          })
        ).to.be.rejectedWith(getVmError("Game is closed."));
      });

      describe("Withdraw", () => {
        it("should reject withdraw attempts if game is still open", async () => {
          await network.provider.send("evm_setNextBlockTimestamp", [
            9999999999999,
          ]);

          await expect(lottie.withdrawStake()).to.be.rejectedWith(
            getVmError("Nothing to withdraw.")
          );
        });

        it("should reject withdraw attempts if there were no players", async () => {
          await network.provider.send("evm_setNextBlockTimestamp", [
            9999999999999,
          ]);

          await lottie.closeGame();
          await expect(
            lottie.connect(alice).withdrawStake()
          ).to.be.rejectedWith(getVmError("Nothing to withdraw."));
        });

        it("should reject withdraw attempts if caller is not the terminator", async () => {
          await lottie.connect(bob).buyTickets(3, {
            value: TICKET_PRICE.mul(3),
          });

          await network.provider.send("evm_setNextBlockTimestamp", [
            9999999999999,
          ]);

          await lottie.closeGame();
          await expect(
            lottie.connect(alice).withdrawStake()
          ).to.be.rejectedWith(getVmError("Permission denied."));
        });

        it("should transfer the staked amount to the terminator", async () => {
          await lottie.connect(bob).buyTickets(3, {
            value: TICKET_PRICE.mul(3),
          });

          await network.provider.send("evm_setNextBlockTimestamp", [
            9999999999999,
          ]);

          await lottie.closeGame();

          const ownerBalanceBeforeRefund = await ethers.provider.getBalance(
            owner.address
          );

          await lottie.withdrawStake();

          const ownerBalanceAfterRefund = await ethers.provider.getBalance(
            owner.address
          );

          // some money lost due to the gas cost
          expect(ownerBalanceAfterRefund)
            .to.be.gt(ownerBalanceBeforeRefund)
            .and.lt(ownerBalanceBeforeRefund.add(STAKE));

          expect(await lottie.isWithdrawable()).to.be.false;

          const terminator = await lottie.terminator();
          expect(terminator[0]).to.eq(owner.address);
          expect(terminator[2]).to.be.gt(9999999999999);
        });
      });
    });
  });
});
