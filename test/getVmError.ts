const getVmError = (error: string) =>
  `VM Exception while processing transaction: reverted with reason string '${error}'`;

export default getVmError;
