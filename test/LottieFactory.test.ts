import chai, { expect } from "chai";
import { Lottie, LottieFactory } from "../types";
import { ethers } from "hardhat";
import { parseEther } from "ethers/lib/utils";
import chaiAsPromised from "chai-as-promised";
import LottieAbi from "../artifacts/contracts/Lottie.sol/Lottie.json";
import getVmError from "./getVmError";

chai.use(chaiAsPromised);

const TICKET_PRICE = parseEther("0.001");
const STAKE = parseEther("0.1");
const ENDS_AT = Date.now() + 60 * 60;
const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

describe("LottieFactory", () => {
  let lottieFactory: LottieFactory;

  beforeEach(async () => {
    const LottieFactory = await ethers.getContractFactory("LottieFactory");
    lottieFactory = await LottieFactory.deploy(ZERO_ADDRESS);
    await lottieFactory.deployed();
  });

  it("should not have any pre-initialized lotties after deployment", async () => {
    expect(await lottieFactory.lottiesCount()).to.eq(0);
  });

  it("should reject creation attempts if expiry date is in the past", async () => {
    await expect(
      lottieFactory.createLottie(TICKET_PRICE, 1, {
        value: STAKE,
      })
    ).to.be.rejectedWith(getVmError("Game should end in the future."));
  });

  it("should create a lottie instance properly", async () => {
    const [owner] = await ethers.getSigners();

    await lottieFactory.createLottie(TICKET_PRICE, ENDS_AT, {
      value: STAKE,
    });

    expect(await lottieFactory.lottiesCount()).to.eq(1);

    const provider = ethers.provider;
    const lottieAddress = await lottieFactory.lotties(0);
    const lottie = new ethers.Contract(
      lottieAddress,
      LottieAbi.abi,
      provider
    ) as Lottie;

    expect(await lottie.isClosed()).to.be.false;
    expect(await lottie.ticketPrice()).to.eq(TICKET_PRICE);
    expect(await lottie.endsAt()).to.eq(ENDS_AT);
    expect(await lottie.soldTicketCount()).to.eq(0);
    expect(await lottie.distinctUserCount()).to.eq(0);
    // the factory transfers the Lottie instance's ownership to the tx signer
    expect(await lottie.owner()).to.eq(owner.address);
  });
});
