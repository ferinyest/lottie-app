/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import { Signer, utils, Contract, ContractFactory, Overrides } from "ethers";
import { Provider, TransactionRequest } from "@ethersproject/providers";
import type { GetRandomMock, GetRandomMockInterface } from "../GetRandomMock";

const _abi = [
  {
    inputs: [],
    name: "random",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
];

const _bytecode =
  "0x608060405234801561001057600080fd5b5060b68061001f6000396000f3fe6080604052348015600f57600080fd5b506004361060285760003560e01c80635ec01e4d14602d575b600080fd5b60336047565b604051603e9190605d565b60405180910390f35b60006009905090565b6057816076565b82525050565b6000602082019050607060008301846050565b92915050565b600081905091905056fea26469706673582212203a93a3b753a5f038e374ff00f353c2826b42d9bba8b694f64a7170f48e23c1be64736f6c63430008050033";

export class GetRandomMock__factory extends ContractFactory {
  constructor(
    ...args: [signer: Signer] | ConstructorParameters<typeof ContractFactory>
  ) {
    if (args.length === 1) {
      super(_abi, _bytecode, args[0]);
    } else {
      super(...args);
    }
  }

  deploy(
    overrides?: Overrides & { from?: string | Promise<string> }
  ): Promise<GetRandomMock> {
    return super.deploy(overrides || {}) as Promise<GetRandomMock>;
  }
  getDeployTransaction(
    overrides?: Overrides & { from?: string | Promise<string> }
  ): TransactionRequest {
    return super.getDeployTransaction(overrides || {});
  }
  attach(address: string): GetRandomMock {
    return super.attach(address) as GetRandomMock;
  }
  connect(signer: Signer): GetRandomMock__factory {
    return super.connect(signer) as GetRandomMock__factory;
  }
  static readonly bytecode = _bytecode;
  static readonly abi = _abi;
  static createInterface(): GetRandomMockInterface {
    return new utils.Interface(_abi) as GetRandomMockInterface;
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): GetRandomMock {
    return new Contract(address, _abi, signerOrProvider) as GetRandomMock;
  }
}
