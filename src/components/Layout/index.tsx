import { FC, forwardRef, ReactElement, useMemo, useState } from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import HomeIcon from "@mui/icons-material/Home";
import HelpIcon from "@mui/icons-material/Help";
import LogoutIcon from "@mui/icons-material/Logout";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import {
  Link as RouterLink,
  LinkProps as RouterLinkProps,
  Route,
  Switch,
} from "react-router-dom";

import Home from "../../pages/Home/Home";
import About from "../../pages/About/About";
import Lotti from "../../pages/Lotti/Lotti";
import NotFound from "../../pages/NotFound/NotFound";
import { AppBar, DrawerHeader, drawerWidth, Main } from "./styles";
import { useMoralis } from "react-moralis";
import truncateEthAddress from "truncate-eth-address";

interface ListItemLinkProps {
  icon?: ReactElement;
  primary: string;
  to: string;
}

const ListItemLink = (props: ListItemLinkProps) => {
  const { icon, primary, to } = props;

  const renderLink = useMemo(
    () =>
      // TODO
      // eslint-disable-next-line no-undef
      forwardRef<HTMLAnchorElement, Omit<RouterLinkProps, "to">>(function Link(
        itemProps,
        ref
      ) {
        return <RouterLink to={to} ref={ref} {...itemProps} role={undefined} />;
      }),
    [to]
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
};

const Layout: FC = () => {
  const {
    authenticate,
    isAuthenticated,
    isAuthenticating,
    user,
    logout,
    isLoggingOut,
  } = useMoralis();
  const theme = useTheme();
  const [open, setOpen] = useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleAuth = () => {
    if (!isAuthenticated) {
      authenticate();
    } else {
      logout();
    }
  };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />

      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{ mr: 2, ...(open && { display: "none" }) }}
          >
            <MenuIcon />
          </IconButton>

          <Typography variant="h6" noWrap component="div">
            Lotti
          </Typography>

          <IconButton
            color="inherit"
            sx={{ ml: "auto", mr: 2 }}
            disabled={isAuthenticating || isLoggingOut}
            onClick={handleAuth}
          >
            {user && isAuthenticated ? (
              <>
                <Typography sx={{ mr: 1 }}>
                  hey, {truncateEthAddress(user.get("ethAddress"))}!
                </Typography>
                <LogoutIcon />
              </>
            ) : (
              <PersonOutlineIcon />
            )}
          </IconButton>
        </Toolbar>
      </AppBar>

      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: drawerWidth,
            boxSizing: "border-box",
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          <ListItemLink to="/" primary="Home" icon={<HomeIcon />} />
          <ListItemLink to="/about" primary="About" icon={<HelpIcon />} />
        </List>
      </Drawer>

      <Main open={open}>
        <DrawerHeader />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/lotti/:address" component={Lotti} />
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </Main>
    </Box>
  );
};

Layout.displayName = "Layout";

export default Layout;
