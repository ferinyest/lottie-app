import { ReactElement, useEffect } from "react";
import { useMoralis } from "react-moralis";
import { BrowserRouter as Router } from "react-router-dom";

import "./App.css";
import Layout from "./components/Layout";

const App = (): ReactElement => {
  const { isAuthenticated, isWeb3Enabled, isWeb3EnableLoading, enableWeb3 } =
    useMoralis();

  useEffect(() => {
    const connectorId = window.localStorage.getItem("connectorId");
    if (isAuthenticated && !isWeb3Enabled && !isWeb3EnableLoading)
      // @ts-ignore
      enableWeb3({ provider: connectorId });
  }, [isAuthenticated, isWeb3Enabled]);

  return (
    <Router>
      <Layout></Layout>
    </Router>
  );
};

export default App;
