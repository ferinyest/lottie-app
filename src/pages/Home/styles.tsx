import { Paper } from "@mui/material";
import { experimentalStyled as styled } from "@mui/material/styles";

export const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
