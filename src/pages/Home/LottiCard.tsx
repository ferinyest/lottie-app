import { FC, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Typography from "@mui/material/Typography";
import { Skeleton, Tooltip } from "@mui/material";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import LockIcon from "@mui/icons-material/Lock";
import TimerOffIcon from "@mui/icons-material/TimerOff";
import { Item } from "./styles";
import truncateEthAddress from "truncate-eth-address";
import { useMoralis, useWeb3Contract } from "react-moralis";
import lotti from "../../abi/Lottie.json";
// eslint-disable-next-line node/no-extraneous-import
import { BigNumber } from "@ethersproject/bignumber";
// eslint-disable-next-line node/no-unpublished-import
import { ethers } from "ethers";

type Props = {
  address: string;
};

type Details = {
  isClosed?: boolean;
  isExpired: boolean;
  endsAt?: string;
  ticketPrice?: string;
  soldTicketsCount?: string;
  userTicketsCount?: string;
  balance?: string;
};

type RawDetails = [
  boolean | undefined,
  BigNumber | undefined,
  BigNumber | undefined,
  BigNumber | undefined,
  BigNumber | undefined,
  BigNumber | undefined
];

const LottiCard: FC<Props> = ({ address }) => {
  const { Moralis, account, isAuthenticated } = useMoralis();
  const [lottiDeatils, setLottiDeatils] = useState<Details | null>(null);
  const { runContractFunction, data } = useWeb3Contract({
    abi: lotti,
    contractAddress: address,
    functionName: "getShortDescription",
  });

  useEffect(() => {
    if (Moralis.web3) {
      const Lotti = new ethers.Contract(
        address,
        lotti,
        // @ts-ignore
        Moralis.web3
      );

      // @ts-ignore
      Lotti.provider.pollingInterval = 500;

      Lotti.on("changed", (...args) => {
        runContractFunction();
      });

      return () => {
        Lotti.removeAllListeners();
      };
    }
  }, [Moralis.web3]);

  useEffect(() => {
    runContractFunction();
  }, [account, isAuthenticated]);

  useEffect(() => {
    if (!data) {
      return;
    }

    const [
      isClosed,
      endsAt,
      ticketPrice,
      soldTicketsCount,
      userTicketsCount,
      balance,
    ] = data as RawDetails;

    const now = Math.floor(Date.now() / 1000);
    const endsAtDate = endsAt
      ? new Date(endsAt.mul(1000).toNumber()).toUTCString()
      : undefined;
    const isExpired = endsAt ? endsAt.toNumber() < now : false;

    const details = {
      isClosed,
      isExpired,
      endsAt: endsAtDate,
      ticketPrice: ticketPrice
        ? ethers.utils.formatEther(ticketPrice)
        : undefined,
      soldTicketsCount: soldTicketsCount?.toString(),
      userTicketsCount: userTicketsCount?.toString(),
      balance: balance ? ethers.utils.formatEther(balance) : undefined,
    };

    setLottiDeatils(details);
  }, [data]);

  const {
    isClosed,
    isExpired,
    endsAt,
    ticketPrice,
    soldTicketsCount,
    userTicketsCount,
    balance,
  } = lottiDeatils ?? {};

  const history = useHistory();
  const handleClick = () => history.push(`/lotti/${address}`);

  const renderStatus = () => (
    <>
      <span>Status:</span>
      {isClosed ? (
        <Tooltip title="Closed" placement="right">
          <LockIcon />
        </Tooltip>
      ) : isExpired ? (
        <Tooltip title="Expired" placement="right">
          <TimerOffIcon />
        </Tooltip>
      ) : (
        <Tooltip title="Open" placement="right">
          <LockOpenIcon />
        </Tooltip>
      )}
    </>
  );

  return (
    <Item onClick={handleClick} sx={{ cursor: "pointer" }}>
      <Typography
        variant="h5"
        component="div"
        sx={{
          overflow: "hidden",
          whiteSpace: "nowrap",
          textOverflow: "ellipsis",
        }}
      >
        {truncateEthAddress(address)}
      </Typography>
      <Typography
        sx={{ mb: 1, mt: 1, display: "inline-flex" }}
        color="text.secondary"
      >
        {isClosed === undefined ? (
          <Skeleton variant="circular" width={24} height={24} />
        ) : (
          renderStatus()
        )}
      </Typography>
      <Typography variant="body2">
        {endsAt ? <span>Ends at: {endsAt}</span> : <Skeleton variant="text" />}
      </Typography>
      <Typography variant="body2">
        {ticketPrice ? (
          <span>Ticket price: {ticketPrice} WAGMI</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
      <Typography variant="body2">
        {balance ? (
          <span>Price pool: {balance} WAGMI</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
      <Typography variant="body2">
        {soldTicketsCount ? (
          <span>Tickets sold: {soldTicketsCount}</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
      <Typography variant="body2">
        {userTicketsCount ? (
          <span>Owned tickets: {userTicketsCount}</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
    </Item>
  );
};

LottiCard.displayName = "LottiCard";

export default LottiCard;
