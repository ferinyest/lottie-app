import React, { FC, useState } from "react";
import Button from "@mui/material/Button";
import {
  Alert,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  InputAdornment,
  Snackbar,
  Stack,
  TextField,
} from "@mui/material";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DateTimePicker from "@mui/lab/DateTimePicker";
import AdapterMoment from "@mui/lab/AdapterMoment";
import moment, { Moment } from "moment";
import LottieFactory from "../../abi/LottieFactory.json";
import { useMoralis, useWeb3ExecuteFunction } from "react-moralis";
import config from "../../config";

enum ToasterStatus {
  NONE,
  SUCCESS,
  ERROR,
}

const CreateLotti: FC = () => {
  const { Moralis } = useMoralis();
  const contractProcessor = useWeb3ExecuteFunction();

  const [open, setOpen] = useState(false);
  const [toasterStatus, setToasterStatus] = useState<ToasterStatus>(
    ToasterStatus.NONE
  );

  const [date, setDate] = useState<Moment | null>(
    moment(new Date(Date.now() + 3600 * 1000 * 24)).local()
  );
  // eslint-disable-next-line no-unused-vars
  const [price, setPrice] = useState<number | null>(1);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async () => {
    if (!date) {
      return;
    }

    const options = {
      contractAddress: config.FACTORY_ADDRESS,
      functionName: "createLottie",
      abi: LottieFactory,
      params: {
        _ticketPrice: Moralis.Units.ETH(price ?? 1),
        _endsAt: Math.floor(date.utc().valueOf() / 1000),
      },
      msgValue: Moralis.Units.ETH(0.1),
    };

    await contractProcessor.fetch({
      params: options,
      onSuccess: () => {
        setToasterStatus(ToasterStatus.SUCCESS);
      },
      onError: () => {
        setToasterStatus(ToasterStatus.ERROR);
      },
    });

    setOpen(false);
  };

  const handleDateChange = (date: Moment | null) => {
    setDate(date);
  };

  const handlePriceChange = (event: any) => {
    setPrice(parseInt(event?.target.value, 10));
  };

  const handleToasterClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setToasterStatus(ToasterStatus.NONE);
  };

  return (
    <Box sx={{ mb: 3, display: "flex", justifyContent: "center" }}>
      <Snackbar
        open={toasterStatus !== ToasterStatus.NONE}
        autoHideDuration={5000}
        onClose={handleToasterClose}
      >
        {toasterStatus === ToasterStatus.SUCCESS ? (
          <Alert severity="success">Lottie successfully created</Alert>
        ) : (
          <Alert severity="error">Error while creating Lottie</Alert>
        )}
      </Snackbar>
      <Button variant="contained" onClick={handleClickOpen}>
        Create new event
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Create new event</DialogTitle>
        <DialogContent>
          <Stack spacing={3}>
            <DialogContentText sx={{ mb: 2 }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec
              quam venenatis, imperdiet odio sit amet, sollicitudin purus.
              Mauris id nisl nec mi consectetur varius. Pellentesque commodo
              volutpat posuere. Etiam eu semper diam. Nunc eu nunc ut nunc
              congue porta. Cras a blandit elit. Etiam mollis sodales tempus. Ut
              eget condimentum sapien. Mauris ac ante leo. In ut tincidunt erat,
              luctus vulputate erat.
            </DialogContentText>
            <TextField
              autoFocus
              onChange={handlePriceChange}
              margin="dense"
              id="name"
              label="Ticket price"
              type="number"
              fullWidth
              variant="standard"
              value={price}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">WAGMI</InputAdornment>
                ),
                inputProps: { min: 1, max: 1000, step: 1 },
              }}
            />

            <LocalizationProvider dateAdapter={AdapterMoment}>
              <DateTimePicker
                label="Expiration date"
                value={date}
                onChange={handleDateChange}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSubmit}>Create</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

CreateLotti.displayName = "CreateLotti";

export default CreateLotti;
