import { FC, useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import LottiCard from "./LottiCard";
import { useMoralis, useWeb3Contract } from "react-moralis";
import lottiFactory from "../../abi/LottieFactory.json";
import config from "../../config";
// eslint-disable-next-line node/no-unpublished-import
import { ethers } from "ethers";

const LottiGrid: FC = () => {
  const { Moralis } = useMoralis();
  const [lottiAddresses, setLottiAddresses] = useState<string[]>([]);
  const { runContractFunction, data } = useWeb3Contract({
    abi: lottiFactory,
    contractAddress: config.FACTORY_ADDRESS,
    functionName: "getLotties",
  });

  // subscribe for events manually, because moralis livesync doesn't work with custom chains (?)
  useEffect(() => {
    const LottiFactory = new ethers.Contract(
      config.FACTORY_ADDRESS,
      lottiFactory,
      // @ts-ignore
      Moralis.web3
    );

    // @ts-ignore
    LottiFactory.provider.pollingInterval = 500;

    LottiFactory.on("created", (...args) => {
      runContractFunction();
    });

    runContractFunction();

    return () => {
      LottiFactory.removeAllListeners();
    };
  }, []);

  useEffect(() => {
    setLottiAddresses((data as string[]) ?? []);
  }, [data]);

  return lottiAddresses ? (
    <Box sx={{ flexGrow: 1 }}>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
      >
        {lottiAddresses.map((address) => {
          return (
            <Grid item xs={2} sm={4} md={4} key={address}>
              <LottiCard address={address} />
            </Grid>
          );
        })}
      </Grid>
    </Box>
  ) : null;
};

LottiGrid.displayName = "LottiGrid";

export default LottiGrid;
