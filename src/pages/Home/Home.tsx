import { Typography } from "@mui/material";
import { FC } from "react";
import { useMoralis } from "react-moralis";
import CreateLotti from "./CreateLotti";
import LottiGrid from "./LottiGrid";

const Home: FC = () => {
  const { isAuthenticated, isWeb3Enabled } = useMoralis();

  return (
    <>
      {isAuthenticated && isWeb3Enabled ? (
        <>
          <CreateLotti />
          <LottiGrid />
        </>
      ) : (
        <Typography>Please log in.</Typography>
      )}
    </>
  );
};

Home.displayName = "Home";

export default Home;
