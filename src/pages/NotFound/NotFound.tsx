import { FC } from "react";

const NotFound: FC = () => <div>NotFound</div>;

NotFound.displayName = "NotFound";

export default NotFound;
