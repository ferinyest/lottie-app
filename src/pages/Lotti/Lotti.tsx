import { FC, useEffect, useState } from "react";
import { Grid } from "@mui/material";
import { useParams } from "react-router-dom";
import Details from "./Details";
// eslint-disable-next-line node/no-extraneous-import
import { BigNumber } from "@ethersproject/bignumber";
// eslint-disable-next-line node/no-unpublished-import
import { ethers } from "ethers";
import { useMoralis, useWeb3Contract } from "react-moralis";
import lotti from "../../abi/Lottie.json";
import Buttons from "./Buttons";

type Winner = {
  addr: string;
  ticketIndex: BigNumber;
  prizeAmount: BigNumber;
  paidAt: BigNumber;
};

type Terminator = {
  addr: string;
  closedAt: BigNumber;
  withdrewAt: BigNumber;
};

type RawDetails = [
  boolean | undefined,
  BigNumber | undefined,
  BigNumber | undefined,
  BigNumber | undefined,
  BigNumber | undefined,
  BigNumber | undefined,
  Winner | undefined,
  Terminator | undefined,
  string | undefined,
  boolean | undefined,
  string | undefined
];

export type LottiDetails = {
  isClosed?: boolean;
  isWithdrawable?: boolean;
  isExpired: boolean;
  isExpiredForOwner: boolean;
  endsAt?: string;
  ticketPrice?: string;
  soldTicketsCount?: string;
  userTicketsCount?: string;
  userCount?: string;
  winner?: Winner;
  terminator?: Terminator;
  owner?: string;
  balance?: string;
};

const Lotti: FC = () => {
  const { address } = useParams<{ address: string }>();
  const { Moralis, isAuthenticated, isWeb3Enabled, account } = useMoralis();
  const [lottiDetails, setLottiDetails] = useState<LottiDetails | null>(null);
  const { runContractFunction, data } = useWeb3Contract({
    abi: lotti,
    contractAddress: address,
    functionName: "getDescription",
  });

  useEffect(() => {
    if (Moralis.web3) {
      const Lotti = new ethers.Contract(
        address,
        lotti,
        // @ts-ignore
        Moralis.web3
      );

      // @ts-ignore
      Lotti.provider.pollingInterval = 500;

      Lotti.on("changed", (...args) => {
        runContractFunction();
      });

      return () => {
        Lotti.removeAllListeners();
      };
    }
  }, [Moralis.web3]);

  useEffect(() => {
    if (isAuthenticated && isWeb3Enabled && account) {
      runContractFunction();
    }
  }, [isAuthenticated, isWeb3Enabled, account]);

  useEffect(() => {
    if (!data) {
      return;
    }

    const [
      isClosed,
      endsAt,
      ticketPrice,
      soldTicketsCount,
      userTicketsCount,
      userCount,
      winner,
      terminator,
      owner,
      isWithdrawable,
      balance,
    ] = data as RawDetails;

    const now = Math.floor(Date.now() / 1000);
    const endsAtDate = endsAt
      ? new Date(endsAt.mul(1000).toNumber()).toUTCString()
      : undefined;
    const isExpired = endsAt ? endsAt.toNumber() < now : false;
    const isExpiredForOwner =
      endsAt && isExpired ? endsAt.toNumber() + 3600 * 1000 * 24 < now : false;

    const details = {
      isClosed,
      isWithdrawable,
      isExpired,
      isExpiredForOwner,
      endsAt: endsAtDate,
      ticketPrice: ticketPrice
        ? ethers.utils.formatEther(ticketPrice)
        : undefined,
      soldTicketsCount: soldTicketsCount?.toString(),
      userTicketsCount: userTicketsCount?.toString(),
      userCount: userCount?.toString(),
      winner,
      terminator,
      owner,
      balance: balance ? ethers.utils.formatEther(balance) : undefined,
    };

    setLottiDetails(details);
  }, [data]);

  if (!isAuthenticated) {
    return null;
  }

  return (
    <Grid container columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
      <Grid item md={8}>
        <Details address={address} details={lottiDetails} />
      </Grid>
      <Grid item md={4}>
        {<Buttons address={address} details={lottiDetails} />}
      </Grid>
    </Grid>
  );
};

Lotti.displayName = "Lotti";

export default Lotti;
