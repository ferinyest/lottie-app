import React, { FC, useState } from "react";
import Button from "@mui/material/Button";
import { Alert, Box, Snackbar } from "@mui/material";
import lotti from "../../abi/Lottie.json";
import { useWeb3ExecuteFunction } from "react-moralis";

type Props = {
  address: string;
};

enum ToasterStatus {
  NONE,
  SUCCESS,
  ERROR,
}

const CloseLotti: FC<Props> = ({ address }) => {
  const contractProcessor = useWeb3ExecuteFunction();
  const [toasterStatus, setToasterStatus] = useState<ToasterStatus>(
    ToasterStatus.NONE
  );

  const closeGame = async () => {
    const options = {
      contractAddress: address,
      functionName: "closeGame",
      abi: lotti,
    };

    await contractProcessor.fetch({
      params: options,
      onSuccess: () => {
        setToasterStatus(ToasterStatus.SUCCESS);
      },
      onError: () => {
        setToasterStatus(ToasterStatus.ERROR);
      },
    });
  };

  const handleToasterClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setToasterStatus(ToasterStatus.NONE);
  };

  return (
    <Box
      sx={{
        mb: 3,
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Snackbar
        open={toasterStatus !== ToasterStatus.NONE}
        autoHideDuration={5000}
        onClose={handleToasterClose}
      >
        {toasterStatus === ToasterStatus.SUCCESS ? (
          <Alert severity="success">Game closed</Alert>
        ) : (
          <Alert severity="error">Error while closing game</Alert>
        )}
      </Snackbar>

      <Button variant="contained" onClick={closeGame}>
        Close game
      </Button>
    </Box>
  );
};

CloseLotti.displayName = "CloseLotti";

export default CloseLotti;
