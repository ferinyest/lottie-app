import { FC } from "react";
import { Typography } from "@mui/material";
import BuyTickets from "./BuyTicket";
import CloseLotti from "./CloseLotti";
import Withdraw from "./Withdraw";
import { useMoralis } from "react-moralis";
import { LottiDetails } from "./Lotti";

type Props = {
  address: string;
  details: LottiDetails | null;
};

const Buttons: FC<Props> = ({ address, details }) => {
  const {
    isClosed,
    isWithdrawable,
    isExpired,
    isExpiredForOwner,
    owner,
    ticketPrice,
  } = details ?? {};
  const { account } = useMoralis();

  const isOwner = owner?.toLowerCase() === account;

  if (isClosed) {
    if (isWithdrawable) {
      return <Withdraw address={address} />;
    } else {
      return null;
    }
  } else if (isExpired) {
    if (isOwner || isExpiredForOwner) {
      return <CloseLotti address={address} />;
    }

    return (
      <Typography>
        Game expired. Waiting for owner to declare winner...
      </Typography>
    );
  }

  return <BuyTickets address={address} price={ticketPrice} />;
};

Buttons.displayName = "Buttons";

export default Buttons;
