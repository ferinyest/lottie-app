import { FC } from "react";
// eslint-disable-next-line node/no-unpublished-import
import { BigNumber, ethers } from "ethers";
import { Divider, Skeleton, Tooltip, Typography } from "@mui/material";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import LockIcon from "@mui/icons-material/Lock";
import TimerOffIcon from "@mui/icons-material/TimerOff";
// eslint-disable-next-line node/no-extraneous-import
import { LottiDetails } from "./Lotti";

type Props = {
  address: string;
  details: LottiDetails | null;
};

const Details: FC<Props> = ({ address, details }) => {
  const {
    isClosed,
    isExpired,
    endsAt,
    ticketPrice,
    soldTicketsCount,
    userTicketsCount,
    userCount,
    winner,
    terminator,
    balance,
  } = details ?? {};

  const hasWinner = winner?.addr !== ethers.constants.AddressZero;
  const hasTerminator = terminator?.addr !== ethers.constants.AddressZero;

  const renderStatus = () => (
    <>
      <span>Status:</span>
      {isClosed ? (
        <Tooltip title="Closed" placement="right">
          <LockIcon />
        </Tooltip>
      ) : isExpired ? (
        <Tooltip title="Expired" placement="right">
          <TimerOffIcon />
        </Tooltip>
      ) : (
        <Tooltip title="Open" placement="right">
          <LockOpenIcon />
        </Tooltip>
      )}
    </>
  );

  return (
    <>
      <Typography
        variant="h5"
        component="div"
        sx={{
          overflow: "hidden",
          whiteSpace: "nowrap",
          textOverflow: "ellipsis",
        }}
      >
        {address}
      </Typography>
      <Typography
        sx={{ mb: 1, mt: 1, display: "inline-flex" }}
        color="text.secondary"
      >
        {isClosed === undefined ? (
          <Skeleton variant="circular" width={24} height={24} />
        ) : (
          renderStatus()
        )}
      </Typography>
      <Typography variant="body2">
        {endsAt ? <span>Ends at: {endsAt}</span> : <Skeleton variant="text" />}
      </Typography>
      <Typography variant="body2">
        {balance ? (
          <span>Price pool: {balance} WAGMI</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
      <Typography variant="body2">
        {ticketPrice ? (
          <span>Ticket price: {ticketPrice} WAGMI</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
      <Typography variant="body2">
        {soldTicketsCount ? (
          <span>Tickets sold: {soldTicketsCount}</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
      <Typography variant="body2">
        {userTicketsCount ? (
          <span>Owned tickets: {userTicketsCount}</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
      <Typography variant="body2">
        {userCount ? (
          <span>Players: {userCount}</span>
        ) : (
          <Skeleton variant="text" />
        )}
      </Typography>
      {hasWinner && (
        <>
          <Divider sx={{ m: 1 }} />
          <Typography variant="body2">Winner: {winner?.addr}</Typography>
          <Typography variant="body2">
            Drew at:{" "}
            {winner?.paidAt
              ? new Date(winner?.paidAt.mul(1000).toNumber()).toUTCString()
              : "no data"}
          </Typography>
          <Typography variant="body2">
            Prize:{" "}
            {winner?.prizeAmount
              ? ethers.utils.formatEther(winner?.prizeAmount.toString()) +
                " WAGMI"
              : "no data"}
          </Typography>
          {
            <Typography variant="body2">
              Ticket number: {winner?.ticketIndex.toString()}
            </Typography>
          }
        </>
      )}
      {hasTerminator && (
        <>
          <Divider sx={{ m: 1 }} />
          <Typography variant="body2">
            Closed by "{terminator?.addr ?? "unknown"}" at{" "}
            {terminator?.closedAt
              ? new Date(
                  terminator?.closedAt.mul(1000).toNumber()
                ).toUTCString()
              : "no data"}{" "}
            stake withdrew at{" "}
            {terminator && !terminator.withdrewAt.eq(BigNumber.from("0"))
              ? new Date(
                  terminator?.withdrewAt.mul(1000).toNumber()
                ).toUTCString()
              : "no data"}
          </Typography>
        </>
      )}
    </>
  );
};

Details.displayName = "Details";

export default Details;
