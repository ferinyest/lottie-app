import React, { FC, useEffect, useState } from "react";
import Button from "@mui/material/Button";
import {
  Alert,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Snackbar,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useMoralis, useWeb3ExecuteFunction } from "react-moralis";
import lotti from "../../abi/Lottie.json";

type Props = {
  address: string;
  price?: string;
};

enum ToasterStatus {
  NONE,
  SUCCESS,
  ERROR,
}

const BuyTickets: FC<Props> = ({ address, price }) => {
  const { Moralis } = useMoralis();
  const contractProcessor = useWeb3ExecuteFunction();
  const [open, setOpen] = useState(false);
  const [toasterStatus, setToasterStatus] = useState<ToasterStatus>(
    ToasterStatus.NONE
  );

  // eslint-disable-next-line no-unused-vars
  const [amount, setAmount] = useState<number>(1);
  const [totalPrice, setTotalPrice] = useState<string | null>(null);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setTotalPrice((amount * parseFloat(price ?? "0")).toFixed(4));
  }, [amount, price]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = async () => {
    setLoading(true);

    const options = {
      contractAddress: address,
      functionName: "buyTickets",
      abi: lotti,
      params: {
        count: amount ?? 0,
      },
      msgValue: Moralis.Units.ETH(totalPrice ?? "0"),
    };

    await contractProcessor.fetch({
      params: options,
      onSuccess: () => {
        setToasterStatus(ToasterStatus.SUCCESS);
      },
      onError: () => {
        setToasterStatus(ToasterStatus.ERROR);
      },
      onComplete: () => {
        setLoading(false);
      },
    });

    setOpen(false);
  };

  const handleAmountChange = (event: any) => {
    setAmount(parseInt(event?.target.value));
  };

  const handleToasterClose = (
    event?: React.SyntheticEvent | Event,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setToasterStatus(ToasterStatus.NONE);
  };

  return (
    <Box sx={{ mb: 3, display: "flex", justifyContent: "center" }}>
      <Snackbar
        open={toasterStatus !== ToasterStatus.NONE}
        autoHideDuration={5000}
        onClose={handleToasterClose}
      >
        {toasterStatus === ToasterStatus.SUCCESS ? (
          <Alert severity="success">Tickets bought, good luck!</Alert>
        ) : (
          <Alert severity="error">Error while buying tickets</Alert>
        )}
      </Snackbar>
      <Button
        variant="contained"
        onClick={handleClickOpen}
        disabled={isLoading}
      >
        Buy tickets
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Buy tickets</DialogTitle>
        <DialogContent>
          <Stack spacing={3}>
            <DialogContentText sx={{ mb: 2 }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec
              quam venenatis, imperdiet odio sit amet, sollicitudin purus.
              Mauris id nisl nec mi consectetur varius. Pellentesque commodo
              volutpat posuere. Etiam eu semper diam. Nunc eu nunc ut nunc
              congue porta. Cras a blandit elit. Etiam mollis sodales tempus. Ut
              eget condimentum sapien. Mauris ac ante leo. In ut tincidunt erat,
              luctus vulputate erat.
            </DialogContentText>
            <TextField
              autoFocus
              onChange={handleAmountChange}
              margin="dense"
              id="name"
              label="Ticket count"
              type="number"
              fullWidth
              variant="standard"
              value={amount ?? 1}
              InputProps={{
                inputProps: { min: 1, max: 1000, step: 1 },
              }}
            />
            <Typography>Total price: {totalPrice || 0} WAGMI</Typography>
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSubmit}>Create</Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

BuyTickets.displayName = "BuyTickets";

export default BuyTickets;
