import ReactDOM from "react-dom";
import { MoralisProvider } from "react-moralis";

import "./index.css";
import App from "./App";
import config from "./config";

ReactDOM.render(
  // @ts-ignore
  <MoralisProvider appId={config.APP_ID} serverUrl={config.SERVER_URL}>
    <App />
  </MoralisProvider>,
  document.getElementById("root")
);
