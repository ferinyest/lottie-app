//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.5;

import "./IGetRandom.sol";

contract GetRandom is IGetRandom {
    address private fetcher;

    constructor(address _fetcher) {
        fetcher = _fetcher;
    }

    function random() external view override returns (uint256) {
        (bool success, bytes memory result) = fetcher.staticcall(
            abi.encodeWithSignature("getRandom()")
        );
        assert(success);
        return abi.decode(result, (uint256));
    }
}
