//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.5;

import "../IGetRandom.sol";

contract GetRandomMock is IGetRandom {
    function random() external view override returns (uint256) {
        return 9;
    }
}
