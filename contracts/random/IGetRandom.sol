//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.5;

interface IGetRandom {
    function random() external view returns (uint256);
}
