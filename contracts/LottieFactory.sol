//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.5;

import "./Lottie.sol";

contract LottieFactory {
    address[] public lotties;
    address randomProvider;

    event created(address lotti);

    constructor(address _randomProvider) {
        randomProvider = _randomProvider;
    }

    function createLottie(uint256 _ticketPrice, uint64 _endsAt)
        external
        payable
    {
        require(_endsAt > block.timestamp, "Game should end in the future.");

        Lottie lottie = (new Lottie){value: msg.value}(
            _ticketPrice,
            _endsAt,
            randomProvider
        );

        address addr = address(lottie);

        Lottie lottieInstance = Lottie(addr);
        lottieInstance.transferOwnership(msg.sender);
        lotties.push(addr);

        emit created(addr);
    }

    function lottiesCount() external view returns (uint256) {
        return lotties.length;
    }

    function getLotties() external view returns (address[] memory) {
        return lotties;
    }
}
