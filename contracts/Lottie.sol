//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "./random/IGetRandom.sol";
import "hardhat/console.sol";

contract Lottie is Ownable {
    using SafeMath for uint256;

    event changed(address lotti);
    address randomProvider;

    // 0.1 as the owner's stake
    uint256 public constant STAKE = 100000000 gwei;

    struct Winner {
        address addr;
        uint256 ticketIndex;
        uint256 prizeAmount;
        uint256 paidAt;
    }

    struct Terminator {
        address addr;
        uint256 closedAt;
        uint256 withdrewAt;
    }

    Winner public winner;
    Terminator public terminator;

    bool public isClosed;
    bool public isWithdrawable;
    uint64 public endsAt;
    uint256 public ticketPrice;
    address[] public tickets;
    address[] public distinctUsers;
    mapping(address => uint256) public userTickets;

    constructor(
        uint256 _ticketPrice,
        uint64 _endsAt,
        address _randomProvider
    ) payable {
        require(msg.value == STAKE, "Incorrect stake amount.");

        ticketPrice = _ticketPrice;
        endsAt = _endsAt;
        randomProvider = _randomProvider;
        isClosed = false;
    }

    modifier isOpen() {
        require(!isClosed, "Game is closed.");
        _;
    }

    // Buy x amount of tickets
    function buyTickets(uint256 count) external payable isOpen {
        require(msg.value == ticketPrice.mul(count), "Ticket price error.");
        require(block.timestamp < endsAt, "Game has expired.");

        for (uint256 i = 0; i < count; i++) {
            tickets.push(msg.sender);
        }

        if (userTickets[msg.sender] == 0) {
            distinctUsers.push(msg.sender);
        }

        userTickets[msg.sender] = userTickets[msg.sender].add(count);
        emit changed(address(this));
    }

    function closeGame() external isOpen {
        if (msg.sender == owner()) {
            require(endsAt < block.timestamp, "Game can not be closed yet.");
        } else {
            require(userTickets[msg.sender] != 0, "Permission denied.");
            require(
                endsAt + 1 days < block.timestamp,
                "Game can not be closed yet."
            );
        }

        isClosed = true;
        terminator = Terminator(msg.sender, block.timestamp, 0);

        // not a popular game I guess...?
        // return the stake to the owner (only the owner can be the msg.sender in this case), nice try anyway!
        if (distinctUsers.length == 0) {
            terminator.withdrewAt = block.timestamp;
            payable(msg.sender).transfer(address(this).balance);
        }
        // send money to the "winner" and the stake to the msg.sender (can be the owner or the single participant)
        else if (distinctUsers.length == 1) {
            selectWinner(0, true);
        }
        // send money to the winner and the stake to the msg.sender (can be the owner or a participant)
        else {
            IGetRandom GetRandom = IGetRandom(randomProvider);
            uint256 randomness = GetRandom.random();
            uint256 winnerIndex = randomness.mod(tickets.length);
            selectWinner(winnerIndex, false);
        }

        emit changed(address(this));
    }

    // TODO send event
    function selectWinner(uint256 winnerIndex, bool canWinFullPrice) private {
        uint256 prizeAmount;
        address winnerAddress = tickets[winnerIndex];

        if (terminator.addr == owner() && !canWinFullPrice) {
            prizeAmount = ((ticketPrice.mul(tickets.length)).mul(99)).div(100);
        } else {
            prizeAmount = ticketPrice.mul(tickets.length);
        }

        winner = Winner(
            winnerAddress,
            winnerIndex,
            prizeAmount,
            block.timestamp
        );

        isWithdrawable = true;

        payable(winnerAddress).transfer(prizeAmount);
    }

    function soldTicketCount() external view returns (uint256) {
        return tickets.length;
    }

    function distinctUserCount() external view returns (uint256) {
        return distinctUsers.length;
    }

    // TODO: send event
    function withdrawStake() external payable {
        require(isWithdrawable, "Nothing to withdraw.");
        require(terminator.addr == msg.sender, "Permission denied.");

        isWithdrawable = false;
        terminator.withdrewAt = block.timestamp;

        payable(msg.sender).transfer(address(this).balance);
        emit changed(address(this));
    }

    function getBalance() public view returns (uint256) {
        return address(this).balance.sub(STAKE);
    }

    function getShortDescription()
        external
        view
        returns (
            bool,
            uint64,
            uint256,
            uint256,
            uint256,
            uint256
        )
    {
        return (
            isClosed,
            endsAt,
            ticketPrice,
            tickets.length,
            userTickets[tx.origin],
            getBalance()
        );
    }

    function getDescription()
        external
        view
        returns (
            bool,
            uint64,
            uint256,
            uint256,
            uint256,
            uint256,
            Winner memory,
            Terminator memory,
            address,
            bool,
            uint256
        )
    {
        return (
            isClosed,
            endsAt,
            ticketPrice,
            tickets.length,
            userTickets[tx.origin],
            distinctUsers.length,
            winner,
            terminator,
            owner(),
            isWithdrawable,
            getBalance()
        );
    }
}
