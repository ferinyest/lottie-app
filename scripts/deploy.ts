import { ethers } from "hardhat";

async function main() {
  // First manually deploy contracts/random/GetRandom.yul (aka fetcher)
  const FETCHER_ADDRESS = "0x03993eb2c7bba76ec834a45368483bdfad336c06";

  const RandomProvider = await ethers.getContractFactory("GetRandom");
  // @ts-ignore
  const randomProvider = await RandomProvider.deploy(FETCHER_ADDRESS);
  console.log(`RandomProvider deployed to ${randomProvider.address}`);

  const LottieFactory = await ethers.getContractFactory("LottieFactory");
  const lottieFactory = await LottieFactory.deploy(randomProvider.address);
  console.log(`LottieFactory deployed to ${lottieFactory.address}`);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
